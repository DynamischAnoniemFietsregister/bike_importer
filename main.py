import requests
import os
import time
import datetime
import psycopg2
import os
import sys
import mbike
import mevent
import logging
import webhook_pusher

import asyncio
import aiohttp

async def get_http(url, from_timestamp, to_timestamp):
    async with aiohttp.ClientSession(timeout=10) as client:
        try:
            
            perfectview_apikey = os.getenv("PERFECTVIEW_APIKEY")
            if not perfectview_apikey:
                logger.debug("Error, you have to setup ENV-variable perfectview_apikey")
                exit()
            payload = {'ApiKey': perfectview_apikey, 'From': from_timestamp, 'To': to_timestamp}
            async with client.get(url, params=payload) as response:
                content = await response.json()
                return content, response.status
        except Exception as e:
            print(e)
            pass

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()




def get_data(loop, from_timestamp, to_timestamp, conn, pusher):
    perfectview_url = "https://vogapi.perfectview.nl/api/Object/GetObjectsWithFrameNumber"
    task = loop.create_task(get_http(perfectview_url, from_timestamp, to_timestamp))
    loop.run_until_complete(task)
    result = task.result()
    if result is None:
        return None
    data, status = task.result()
    if status != 200:
        print(status)
        return None
    
    

    result_bikes = {}
    bikes = data["Objects"]
    for p_bike in bikes:
        bike = mbike.Bike.check_perfectview_bike(p_bike, conn)
        if bike.last_event != None:
            pusher.enqueue(bike)
        result_bikes[bike.brand + ':' + bike.frame_number] = bike
    return result_bikes

def load_data_database(conn):
    cur = conn.cursor()
    stmt = """
        SELECT bike.bike_id, brand, frame_number
        FROM bike
        JOIN 
            (SELECT events.bike_id
            FROM events
            JOIN
                (SELECT bike_id, max(event_id) as max_event_id 
                FROM events
                GROUP BY bike_id) as q1
            ON q1.max_event_id = events.event_id
            WHERE event_type = 'check_in_depot') as checked_in_bikes
        ON bike.bike_id = checked_in_bikes.bike_id;
        """
    cur.execute(stmt)
    
    rows = cur.fetchall()
    bikes = {}
    for db_bike in rows:
        bikes[db_bike[1] + ':' + db_bike[2]] = mbike.Bike(db_bike[0], db_bike[1], db_bike[2])
    return bikes

conn_str = "dbname='daf'"

if "IP" in os.environ:
    conn_str += " host={} ".format(os.environ['IP'])
if "DB_PASSWORD" in os.environ:
    conn_str += " user=daf password={}".format(os.environ['DB_PASSWORD'])

conn = psycopg2.connect(conn_str)

start_time = '2019-01-01 00:00'
now = datetime.datetime.now()
end_time = now.strftime('%Y-%m-%d %H:%M')

# Load previous_import with data that is already in db.
previous_import = load_data_database(conn)
pusher = webhook_pusher.WebHookPusher()
pusher.start()


loop = asyncio.get_event_loop()

while True:
    start = time.time()
    logger.debug("Start importing bikes perfectview.")
    now = datetime.datetime.now()
    to_timestamp = now.strftime('%Y-%m-%d %H:%M')
    current_import = get_data(loop, start_time, to_timestamp, conn, pusher)

    if current_import != None:
        # Checkout all bikes that were deleted from the feed.
        deleted_bikes = {k:v for k,v in previous_import.items() if k not in current_import}
        for bike in deleted_bikes:
            previous_import[bike].check_out_depot(conn)
            if previous_import[bike].last_event != None:
                pusher.enqueue(previous_import[bike])
        previous_import = current_import

    delta = time.time() - start
    logger.debug("Import took: {:2.2f} s".format(delta))
    if delta < 60.0:
        time.sleep(60 - delta)
