import mevent
import logging

class Bike:
    def __init__(self, bike_id, frame_number, brand):
        self.bike_id = bike_id
        self.frame_number = frame_number
        self.brand = brand
        self.last_event = None

    @staticmethod
    def check_perfectview_bike(perfectview_bike, conn):
        cur = conn.cursor()
        bike = None
        frame_number = perfectview_bike["FrameNumber"]
        brand = perfectview_bike["Brand"]

        cur.execute("""
        SELECT bike_id, frame_number, brand
        FROM bike
        WHERE frame_number = %s
        AND brand = %s""", (frame_number, brand))

        records = cur.fetchall()
        if len(records) > 1:
            print("More than one bicycle with the framenumber + brand combination!")

        if len(records) > 0:
            data = records[0]
            bike = Bike(data[0], data[1], data[2])
        else:
            cur.execute("""INSERT INTO bike 
                (frame_number, brand, created_at, updated_at) 
                VALUES (%s, %s, NOW(), NOW())
                returning bike_id""", (frame_number, brand))
            bike_id = cur.fetchone()[0]
            bike = Bike(bike_id, frame_number, brand)

        bike.check_in_depot(conn, perfectview_bike)

        conn.commit()
        cur.close()
        return bike

    def check_in_depot(self, conn, data):
        if not mevent.Event.is_checked_in(self.bike_id, conn):
            logger = logging.getLogger()
            logger.debug("check_in: " + str(self))
            self.last_event = mevent.Event.check_in(self.bike_id, conn, data)

    def check_out_depot(self, conn):
        if not mevent.Event.is_checked_out(self.bike_id, conn):
            logger = logging.getLogger()
            logger.debug("check_out: " + str(self))
            self.last_event = mevent.Event.check_out(self.bike_id, conn)
        
        
    def __str__(self):
        return ("bike_id: " + str(self.bike_id) + "\n"
            + "brand: " + self.brand + "\n"
            + "frame_number: " + str(self.frame_number) + "\n")
            

