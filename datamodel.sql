-- This entitiy respresents a registered bike in the register
-- In the initial version this table is automatically filled.
-- In the future owners of a bicycle may can register their bike in the register them selves. 
CREATE TABLE bike (
	bike_id INT GENERATED ALWAYS AS IDENTITY,
        frame_number VARCHAR(255),
        brand VARCHAR(255),
	created_at timestamp NOT NULL DEFAULT NOW(),
        updated_at timestamp NOT NULL DEFAULT NOW(),
	PRIMARY KEY(bike_id)
);

CREATE TABLE events (
        event_id INT GENERATED ALWAYS AS IDENTITY,
	bike_id INT REFERENCES bike(bike_id),
        event_type VARCHAR(50) NOT NULL,
        extra_information TEXT,
	timestamp timestamp NOT NULL DEFAULT NOW(),
        PRIMARY KEY(event_id)
);
