FROM python:3.7-alpine
  
RUN apk update && apk add libpq postgresql-dev gcc musl-dev libffi-dev tzdata

COPY requirements.txt /
RUN pip install -r /requirements.txt

COPY . /app
WORKDIR /app

CMD [ "python", "./main.py" ]
