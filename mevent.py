import datetime

class Event():
    def __init__(self, bike_id, event_type, description, timestamp):
        self.event_id = None
        self.bike_id = bike_id
        self.event_type = event_type
        self.description = description
        self.timestamp = timestamp

    def save(self, conn):
        cur = conn.cursor()
        
        cur.execute("""INSERT INTO events
            (bike_id, event_type, extra_information, timestamp)
            VALUES (%s, %s, %s, %s) 
            RETURNING event_id""", (self.bike_id, self.event_type, self.description, self.timestamp))
        conn.commit()
        self.event_id = cur.fetchone()[0]
        cur.close()
        return self

    @staticmethod
    def check_in(bike_id, conn, data):
        timestamp = datetime.datetime.strptime( data["Date"], "%d-%m-%Y %H:%M:%S")
        event = Event(bike_id, "check_in_depot", data["Description"], timestamp)
        return event.save(conn)

    @staticmethod
    def check_out(bike_id, conn):
        timestamp = datetime.datetime.now()
        event = Event(bike_id, 'check_out_depot', None, timestamp)
        return event.save(conn)

    @staticmethod
    def is_checked_in(bike_id, conn):
        cur = conn.cursor()
        
        cur.execute("""
        SELECT event_type 
        FROM events
        WHERE bike_id = %s
        AND (event_type = 'check_in_depot' or event_type = 'check_out_depot')
        ORDER BY event_id DESC
        limit 1
        """, (bike_id,))
        
        records = cur.fetchall()
        if len(records) > 0 and records[0][0] == "check_in_depot":
            return True
        return False

    @staticmethod
    def is_checked_out(bike_id, conn):
        cur = conn.cursor()
        
        cur.execute("""
        SELECT event_type 
        FROM events
        WHERE bike_id = %s
        AND (event_type = 'check_in_depot' or event_type = 'check_out_depot')
        ORDER BY event_id DESC
        limit 1
        """, (bike_id,))
        
        records = cur.fetchall()
        if len(records) > 0 and records[0][0] == "check_out_depot":
            return True
        return False

    def get_webhook_json(self, bike):
        data = {}
        data["event_id"] = self.event_id
        data["event_type"] = self.event_type
        data["extra_information"] = self.description
        data["timestamp"] = self.timestamp.strftime("%Y-%m-%dT%H:%M:%SZ")
        data["bike"] = {}
        data["bike"]["bike_id"] = bike.bike_id
        data["bike"]["brand"] = bike.brand
        data["bike"]["frame_number"] = bike.frame_number
        return data

